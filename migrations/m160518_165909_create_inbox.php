<?php

use yii\db\Migration;

/**
 * Handles the creation for table `inbox`.
 */
class m160518_165909_create_inbox extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('inbox', [
            'id' => $this->primaryKey(),
            'from_number' => $this->string(20)->notNull(),
            'message' => $this->text()->notNull(),
            'status' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('inbox');
    }
}
