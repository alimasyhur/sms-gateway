<?php

use yii\db\Migration;

/**
 * Handles the creation for table `outbox`.
 */
class m160518_165916_create_outbox extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('outbox', [
            'id' => $this->primaryKey(),
            'to' => $this->string(20)->notNull(),
            'message' => $this->text()->notNull(),
            'status' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('outbox');
    }
}
