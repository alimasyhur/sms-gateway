<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inbox".
 *
 * @property integer $id
 * @property string $from_number
 * @property string $message
 * @property string $status
 */
class Inbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_number', 'message'], 'required'],
            [['message'], 'string'],
            [['from_number'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_number' => 'From Number',
            'message' => 'Message',
            'status' => 'Status',
        ];
    }
}
