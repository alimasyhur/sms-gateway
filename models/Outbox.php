<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "outbox".
 *
 * @property integer $id
 * @property string $to
 * @property string $message
 * @property string $status
 */
class Outbox extends \yii\db\ActiveRecord
{
    const STATUS_SUCCESS = 0;
    const STATUS_THROTTLED = 1;
    const STATUS_MISSING_PARAMS = 2;
    const STATUS_INVALID_PARAMS = 3;
    const STATUS_INVALID_CREDENTIALS = 4;
    const STATUS_INTERNAL_ERROR = 5;
    const STATUS_INVALID_MESSAGE = 6;
    const STATUS_NUMBER_BARRED = 7;
    const STATUS_PARTNER_ACCOUNT_BARRED = 8;
    const STATUS_PARTNER_QUOTA_EXCEEDED = 9;
    const STATUS_ACCOUNT_NOT_ENABLE_FOR_REST = 11;
    const STATUS_MESSAGE_TOO_LOG = 12;
    const STATUS_COMMUNICATION_FAILED = 13;
    const STATUS_INVALID_SIGNATURE = 14;
    const STATUS_INVALID_SENDER_ADDRESS = 15;
    const STATUS_INVALID_TTL = 16;
    const STATUS_FACILITY_NOT_ALLOWED = 19;
    const STATUS_INVALID_MESSAGE_CLASS = 20;
    const STATUS_BAD_CALLBACK = 23;
    const STATUS_NON_WHITELIST_DESTINATION = 29;




    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to', 'message'], 'required'],
            [['message'], 'string'],
            [['to'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'to' => 'To',
            'message' => 'Message',
            'status' => 'Status',
            'statusText' => Yii::t('app', 'Status'),

        ];
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_SUCCESS => 'Success',
    self::STATUS_THROTTLED => 'Throttled',
    self::STATUS_MISSING_PARAMS => 'Missing Params',
    self::STATUS_INVALID_PARAMS => 'Invalid Params',
    self::STATUS_INVALID_CREDENTIALS => 'Invalid Credentials',
    self::STATUS_INTERNAL_ERROR => 'Internal Error',
    self::STATUS_INVALID_MESSAGE => 'Invalid Message',
    self::STATUS_NUMBER_BARRED => 'Number Barred',
    self::STATUS_PARTNER_ACCOUNT_BARRED => 'Partner Account Barred',
    self::STATUS_PARTNER_QUOTA_EXCEEDED => 'Partner Quota Exceeded',
    self::STATUS_ACCOUNT_NOT_ENABLE_FOR_REST => 'Account Not Enable For REST',
    self::STATUS_MESSAGE_TOO_LOG => 'Message Too Long',
    self::STATUS_COMMUNICATION_FAILED => 'Communication Failed',
    self::STATUS_INVALID_SIGNATURE => 'Invalid Signature',
    self::STATUS_INVALID_SENDER_ADDRESS => 'Invalid Sender Address',
    self::STATUS_INVALID_TTL => 'Invalid TTL',
    self::STATUS_FACILITY_NOT_ALLOWED => 'Facility Not Allowed',
    self::STATUS_INVALID_MESSAGE_CLASS => 'Invalid Message Class',
    self::STATUS_BAD_CALLBACK => 'Bad Callback',
    self::STATUS_NON_WHITELIST_DESTINATION => 'Non Whitelist Destination',
        ];
    }

    public function getStatusText()
    {
        $data = $this->getStatusOptions();
        return isset($data[$this->status]) ? $data[$this->status] : "Unknown";
    }
}


