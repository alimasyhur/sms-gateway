<?php

namespace app\controllers;

use Yii;
use app\models\Member;
use app\models\MemberSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MemberController implements the CRUD actions for Member model.
 */
class MemberController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Member models.
     * Get parameters Inbound Message
     * @return mixed
     */
    public function actionIndex()
    {
        $data = Yii::$app->request->get();

        $this->checkAndSaveNumber($data);

        $searchModel = new MemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Get Inbound Message and save it into database
     * @param $data
     * @return true
     */
    private function checkAndSaveNumber($data)
    {
        if (isset($data['msisdn'])) {
            $phone_number = $data['msisdn'];
            $this->checkNumberIsExist($phone_number);
        }
        return true;
    }

    /**
     * Check Number whether it exist or not in the model
     * @param $phone_number
     * @return bool
     */
    private function checkNumberIsExist($phone_number)
    {
        $model = Member::find()->where(['phone_number' => $phone_number])->one();
        if (!is_null($model)) {
            return true;
        } else {
            $new_member = new Member();
            $new_member->phone_number = $phone_number;
            return $new_member->save();
        }
    }


    /**
     * Displays a single Member model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Member model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Member the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Member::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
