<?php

namespace app\controllers;

use Yii;
use app\models\Outbox;
use app\models\OutboxSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use dosamigos\nexmo;
use dosamigos\nexmo\Sms;
/*use Nexmo\Developer;
use Nexmo\Insight;
use Nexmo\Sms;
use Nexmo\Verify;
use Nexmo\Voice;*/

/**
 * OutboxController implements the CRUD actions for Outbox model.
 */
class OutboxController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Outbox models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OutboxSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Outbox model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Outbox();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $to = $model->to;
            $message = $model->message;
            $id = $model->id;
            $result = $this->sendMessage($to, $message);
            $status = $result['messages'][0]['status'];
            $this->update($id, $status);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function update($id, $status)
    {
        $model = $this->findModel($id);
        $model->status = $status;

        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }


    protected function sendMessage($to, $message)
    {
        $sms = new Sms($this->configuration());

        return $sms->sendText($to, $message);

        /*$config = $this->configuration();
        $sms = new Sms($config);

        return $sms->send([
            'from' => $config['from'],
            'to' => $to,
            'text' => $message,
        ]);*/
    }

    protected function configuration()
    {
        return [
            'key' => 'e0d02234',
            'secret' => '026690c7ed7bb2ae',
            'from' => '6285574670760',
            /*'api_key' => 'e0d02234',
            'api_secret' => '026690c7ed7bb2ae',
            'from' => '6285574670760',*/
            /*'to' => '',
            'text' => '',
            'search' => [
                'id' => '',
            ],*/
        ];
    }

    /**
     * Deletes an existing Outbox model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Outbox model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Outbox the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Outbox::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
